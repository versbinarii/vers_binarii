If you want to load your source code into the microcontroller from your Linux development host then look no further... open OCD is the tool for you. The instruction in here are only applicable to linux OS.

Ok, so lets start... You can download the openocd from openOCD main [page](http://openocd.org/) or by executing below command:
``` bash
wget http://sourceforge.net/projects/openocd/files/openocd/0.9.0-rc1/openocd-0.9.0-rc1.tar.bz2
```
Then simply unpack the downloaded file with:
```bash
tar xjfv openocd-0.9.0-rc1.tar.bz2\r\ncd openocd-0.9.0-rc1
```
In there you can/should read the README file as it describes some additional options, like binary prefix/suffix or location, you can use while compiling the source. I dont need any of the extra stuff so i simply do:
```bash
./configure
```
Read the output as it will tell you what packages you might be missing that are required for installation. At the end of the output you will most likely see something like that:
```bash
OpenOCD configuration summary
--------------------------------------------------
MPSSE mode of FTDI based devices        no
ST-Link JTAG Programmer                 no
TI ICDI JTAG Programmer                 no
Keil ULINK JTAG Programmer              no
Altera USB-Blaster II Compatible        no
Versaloon-Link JTAG Programmer          no
Segger J-Link JTAG Programmer           no
OSBDM (JTAG only) Programmer            no
eStick/opendous JTAG Programmer         no
Andes JTAG Programmer                   no
USBProg JTAG Programmer                 no
Raisonance RLink JTAG Programmer        no
Olimex ARM-JTAG-EW Programmer           no
CMSIS-DAP Compliant Debugger            no
```
It most likely just means you are missing the libusb libraries on your system. On fedora you can easily install it with:
```bash
sudo yum install libusb-devel libusbg libusb
```
After installation just run `./configure` again and you should see:
```bash
OpenOCD configuration summary
--------------------------------------------------
MPSSE mode of FTDI based devices        yes (auto)
ST-Link JTAG Programmer                 yes (auto)
TI ICDI JTAG Programmer                 yes (auto)
Keil ULINK JTAG Programmer              yes (auto)
Altera USB-Blaster II Compatible        yes (auto)
Versaloon-Link JTAG Programmer          yes (auto)
Segger J-Link JTAG Programmer           yes (auto)
OSBDM (JTAG only) Programmer            yes (auto)
eStick/opendous JTAG Programmer         yes (auto)
Andes JTAG Programmer                   yes (auto)
USBProg JTAG Programmer                 yes (auto)
Raisonance RLink JTAG Programmer        yes (auto)
Olimex ARM-JTAG-EW Programmer           yes (auto)
CMSIS-DAP Compliant Debugger            no
```
Then you can just type:
```bash
make
sudo make install
```
The last command will install the binaries in the specified path, by default it's `/usr/local/bin`.
Now, notice a `tcl/` directory in the openocd folder. You can copy it to somewhere easily accessible as you will be accessing it often. It contains the configuration files for different target (microcontrolers or dev boards) and interface configs ( olimex, st-link, u-link). So assuming that you are in the tcl/ directory and that your stm32Discovery board is connected to laptop you should be able to run:
```bash
sudo openocd -f board/stm32f4discovery.cfg -f interface/stlink-v2.cfg
```
And you should see:
```bash
[kris@facehugger tcl]$ sudo  openocd -f board/stm32f4discovery.cfg -f interface/stlink-v2.cfg
Open On-Chip Debugger 0.9.0-rc1 (2015-05-17-22:22)
Licensed under GNU GPL v2
For bug reports, read
http://openocd.org/doc/doxygen/bugs.html
Info : The selected transport took over low-level target control. The results might differ compared to plain JTAG/SWD
adapter speed: 2000 kHz
adapter_nsrst_delay: 100
none separate
srst_only separate srst_nogate srst_open_drain connect_deassert_srst
Warn : Interface already configured, ignoring
Error: already specified hl_layout stlink
Info : Unable to match requested speed 2000 kHz, using 1800 kHz
Info : Unable to match requested speed 2000 kHz, using 1800 kHz
Info : clock speed 1800 kHz
Info : STLINK v2 JTAG v17 API v2 SWIM v0 VID 0x0483 PID 0x3748
Info : using stlink api v2
Info : Target voltage: 2.897336
Info : stm32f4x.cpu: hardware has 6 breakpoints, 4 watchpoints
```
Dont worry about the warnings. We will fix them someday when we learn more about the software. Then from different console window type in:
```bash
telnet localhost 4444
```
And you should see:
```bash
[kris@facehugger stm32_dev]$ telnet localhost 4444
Trying 127.0.0.1...
Connected to localhost.
Escape character is ''^]''.
Open On-Chip Debugger
>
```
There are tons of commands to execute so  I advice to check with the openOCD manual that can be downloaded [here](http://sourceforge.net/projects/openocd/files/openocd/0.9.0-rc1). But just as a quick test run:
```bash
> reset halt
> reset run
```
Will stop/start the loaded program execution.


