<h2 id="lets-do-this"> Lets do this</h2>

Following is the more less complete process of installing Arch Linux with Sway
on a Lenovo T480 laptop. The Arch Linux [wiki][3] contains a vast amounts of information 
and giudes on th instalation process, however the amount of information might be
sligthly overwhelming. While the Arch installation itself is quite straightforward 
the remaining configuration tasks might be a little more challenging.


<h2 id="network-setup"> Network setup</h2>

Burn the Arch linux [ISO][1] onto the USB drive. 
The T480 came with preinstalled Windows so enable legacy UEFI support in BIOS.
Plug in the USB drive with the Arch ISO. 

The T480 comes with an Eth port so you can plug it into a router. I did not have
an access to a router so i had to use WiFi access.

Connect to Wifi using: 

```bash
wpa_supplicant -B -i wlan0 -c <(wpa_passphrase <SSID> <KEY>)
```

Specify correct SSID and KEY to access the WiFi. To make sure youre connected type:
In my case the WiFi interface on T480 was wlan0. You can find out what it is
with: `ip link`.

Command `iw dev wlan0 link` should state that you are connected. 
If not validate if SSID and KEY are correct.
So we are now conncted to the WiFi but the network stack is still down. So
if you try to ping i.e. 8.8.8.8 you wont be able to.

Command `ip address show` will confirm that there is in fact no IP assigned to the interface.
You can assign a static IP to the interface but since you are already on WiFi 
there is a DHCP server running on the router. Use `dhclient wlan0` to obtain IP 
through DHCP. At this stage you should be able to ping 8.8.8.8. Make sure you 
can resolve DNS by trying to ping google.com. If you cannot ping it then the 
quickest way to get DNS resolution working is to add: `nameserver 8.8.8.8` to the '/etc/resolv.conf'
file - `google.com` should now be pingable.

Follow the Arch wiki guide and configure the time: 

<h2 id="set-up-partitions"> Set up partitions</h2>

I want to set up following partitions:
 * /boot (sda1)
 * swap (sda2)
 * / (sda3)
 * /home (sda4)

When creating the boot partition make sure to specify the EFI boot type.
Then create the file systm for EFI boot partition:
```bash
mkfs.fat -F32 /dev/sda1
```
Its sda1 since its the first partition created.
Create the file system on remaining partions and initialize swap:
```bash
mkfs.ext4 /dev/sda3
mkfs.ext4 /dev/sda4
mkswp /dev/sda2
swapon /dev/sda2
```

Now mount all the partitions in this order:
```bash
mount /dev/sda3 /mnt
mount /dev/sda4 /mnt/home
```
<h2 id="configure-the-system"> Configure the system</h2>

At this stage install any package you might need. For me it was enough to install 
the following:
```
pacstrap /mnt base linux linux-firmware iproute2 vim dhclient networkmanager man 
```

Now generate the /etc/fstab file:
```
genfstab -U /mnt >> /mnt/etc/fstab
```
And change your root directory into /mnt:
```
arch-root /mnt
```
Set time zone, since i'm in Poland my time zone is: 
`ln -sf /usr/share/zoneinfo/Europe/Warsaw /etc/localtime`


Create the /etc/locale.conf file, and set the LANG variable accordingly:
`LANG=en_US.UTF-8`

In `/etc/hostname` specify a name that is going to be your latops hostname: `facehugger`.

Then in `/etc/hosts`
```
127.0.0.1	localhost
::1		localhost
127.0.1.1	facehugger.localdomain	facehugger
```

Set up the root password with `passwr`. This password will be the one you will use to log in after reboot.

<h2 id="install-bootloader"> Install bootloader</h2>

I will use Grub bootloader for this. Since we decided to use UEFI we also need efibootmgr.
Install them both: 
```
pacman -S grub efibootmgr
```
Create the boot directory:
```
mkdir /boot/efi
```

Install the EFI application and generate the grub config thats qoing to be used during boot:
```
grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg
```
Any further changes to the GRUB setting will requre the grub.cfg to be generated again.
We should be now ready to reboot the system. When the laptop starts again it shlod be
booting the system from the SSD since thats your Arch linux up and running.

<h2 id="after-reboot"> After reboot</h2>

You are greeted with just a black screen with a login prompt. Not really impressive but
at least it means your installation was succesfull. Login as root and the password you specified earlier.
You most likely dont want to operate the system as root so create a user for your day to day tasks.
Install the sudo utility:
```
pacman -S sudo
```
Then create a user who is a member of the `wheel` group:
```
useradd -m -G wheel kris
```
Members of `wheel` group are allowed to elevate their priviledges with sudo.


<h2 id="wayland"> Wayland</h2>

For a long time i really wanted to try out Wayland so thats what i will use for this setup.
Since Wayland is just a protocol we need an actual compositor. Currently the only 
realistic choice is between Weston and Sway. We will use [Sway][2] as in my 
my understanding Weston is not really fit into day to day usage.

<h3 id="sway"> Sway </h3>

Install Sway along with few other usefull modules:
```
pacman -S sway i3status swayidle swaylock alacritty
```
* `sway` - is of course the copositor
* `i3status` - is the replacement staus bar to the default swaybar (you can skip that if you wish)
* `swayidle` and `swaylock` - are tools that will disable and lock your display
when maxhine becomes idle.
* `Alacritty` - is a Wayland native terminal written in Rust. Its great it 
comes as the default terminal as i have been using it for quite some time and loving it.

Once installed you should be able to run `sway` in console to start the UI.
There wont be much there initialy until we adjust the configuration file.

To quit `sway` press Logo key + Shift + e because Sway uses the Logo key as its 
modifier by default (you can change it later to whatever else you want).


Sway allows you to use your existing i3 configuration file if you were using i3. 
I'm moving from MacOS so i have no existing i3config. We will copy Sway example 
config file from `/etc/sway/config` into `~/.confg/sway/config` 
(as recommended on Sway wiki) and will modify it as we need it.

<h3 id="displays"> Displays</h3>

In my setup i have another external monitor so we need to tell sway what mode we want to display it in.
My monitor is hanging on an arm and its actually rotate 180 degrees to fit better on my desk.

You can find plenty of info in 'man 5 sway' and 'man 5 sway-output'.

Show available displays: `swaymsg -t get_outputs` 
```
Output eDP-1 'Chimei Innolux Corporation 0x14C9 0x00000000' (focused)
  Current mode: 1920x1080 @ 60.007999 Hz
  Position: 0,0
  Scale factor: 1.000000
  Scale filter: nearest
  Subpixel hinting: unknown
  Transform: normal
  Workspace: 1
  Max render time: off
  Available modes:
    1920x1080 @ 60.007999 Hz

Output HDMI-A-2 'Samsung Electric Company C24F390 H4ZM913536'
  Current mode: 1920x1080 @ 60.000000 Hz
  Position: 1920,0
  Scale factor: 1.000000
  Scale filter: nearest
  Subpixel hinting: unknown
  Transform: 180
  Workspace: 2
  Max render time: off
  Available modes:
    720x400 @ 70.082001 Hz
    640x480 @ 59.939999 Hz
    640x480 @ 60.000000 Hz
    640x480 @ 66.667000 Hz
    640x480 @ 72.808998 Hz
    720x480 @ 59.939999 Hz
    720x480 @ 59.939999 Hz
    720x480 @ 60.000000 Hz
    720x480 @ 60.000000 Hz
    720x576 @ 50.000000 Hz
    720x576 @ 50.000000 Hz
    800x600 @ 56.250000 Hz
    800x600 @ 60.317001 Hz
    800x600 @ 72.188004 Hz
    1024x768 @ 60.004002 Hz
    1024x768 @ 70.069000 Hz
    1280x720 @ 50.000000 Hz
    1280x720 @ 50.000000 Hz
    1280x720 @ 59.939999 Hz
    1280x720 @ 60.000000 Hz
    1280x720 @ 60.000000 Hz
    1280x800 @ 59.910000 Hz
    1440x900 @ 59.901001 Hz
    1280x1024 @ 60.020000 Hz
    1600x900 @ 60.000000 Hz
    1680x1050 @ 59.882999 Hz
    1920x1080 @ 50.000000 Hz
    1920x1080 @ 59.939999 Hz
    1920x1080 @ 60.000000 Hz
    1920x1080 @ 60.000000 Hz
```
Above is the output of the command with my external monitor plugged in. So from the 
output the laptop display config becomes:
```bash
output eDP-1 {
	resolution 1920x1080 
	position 0,0
}
```
for the default display and:
```bash
output HDMI-A-2 {
	resolution 1920x1080 
	position 1920,0
	transform 180
}
```
for my external monitor. Notice the position starting at 1920. This makes the 
external monitor to be on the right. The `transform 180` option rotates the picture
by 180 degrees because of how i mounted the monitor on my desk.


<h3 id="idle-settings"> Idle settings </h3>

Uncomment the following section to configure the idle locking:
```
 exec swayidle -w \
          timeout 300 'swaylock -f -c 000000' \
          timeout 600 'swaymsg "output * dpms off"' \
               resume 'swaymsg "output * dpms on"' \
          before-sleep 'swaylock -f -c 000000'

```
You can adjust the timouts values as you see fit.

<h3 id="sound"> Sound</h3>

This is the easiest step. All that was needed here was to install:
```
pacman -S pulseaudio pulseaudio-alsa alsa-utils
```
After installation and a reboot the stuff just worked so i did not have to mess 
with this at all

<h3 id="function-buttons"> Function buttons</h3>

Lenovo T480 comes with a bunch of function keys that control the display brightness and Volume.
Lets tell Sway what to do when they are pressed:
```
    # Add volume control
    bindsym XF86AudioMute exec amixer -q sset Master toggle
    bindsym XF86AudioLowerVolume exec amixer -q sset Master 3%-
    bindsym XF86AudioRaiseVolume exec amixer -q sset Master 3%+

    # Add brigthness control
    bindsym XF86MonBrightnessUp exec light -A 20
    bindsym XF86MonBrightnessDown exec light -U 20

```
This tells Sway to execute those programs whenever corresponding buttons are pressed.
You can validate what key codes those are with [`xev`][4].
You also need to install `light` for controlling the backlight. `amixer` should 
already be present as it comes within the alsa-utils pacakge.
```
packman -S light
```
<h3 id="clipboard"> Clipboard</h3>

Sway comes with a basic clipboard manager. 
There is also native Wayland [`wl-clipboard`][5] that you can integrate with vim.
Just add the following to your .vimrc:
```
noremap <leader>c :w !wl-copy <cr><cr>
noremap <leader>v :read !wl-paste <cr><cr>
```
So with your leader key being the Space key you can easily copy and paste with 
Space+c/Space+v

<h2 id="x11-compatibility"> X11 compatibility</h2>

The downside of Sway is that no X11 program will run. So if you will install 
Firefox and try to run it it will die with an error: `Cannot open display`. 
You can install a `Xwayland` which allows you to run X11 programs under Sway. 

<h2 id="notifications"> Notifications</h2>

There is nothing out of the box in sway to handle notifications. What i found to
be working wel enough for me is `mako`. 
```
sudo pacman -S mako
```

<h2 id="wrapping-up"> Wrapping up</h2>

At this stage you should have a more less complete Arch linux installation with 
a Wayland based UI. So far the experience is quite enjoyable. There is however 
plenty of config changes and customisations to be made. The sway configuration
files list overwhelming number of different configuration options. Also one could 
configure Sway to start at boot. I do not need this yet as i might want to install 
Xserver and would like to select if i want to run it or Sway. One might potentialy 
add a login screen. 


[1]: https://www.archlinux.org/download/
[2]: https://github.com/swaywm/sway/wiki
[3]: https://wiki.archlinux.org/index.php/Installation_guide
[4]: https://wiki.archlinux.org/index.php/Keyboard_input#Identifying_keycodes_in_Xorg
[5]: https://github.com/bugaevc/wl-clipboard
