### Note: ###

#### __This is a bunch of old blog posts i recovered from old website. Some of the information in there might not be longer correct.__ ####

Ok, so so far we have managed to get a simple LED flashing project to well...flash and last time we added some code so that we are now able to read the state of a pin. Now i wanted to show how to configure the stm32 to generate interrupt on the pin state change but maybe before we do that lets firs investigate how to configure the stm32f4 to use the external crystal to generate a precise system clock. I'm not gonna explain here pros and cons of internal vs external oscillator in embedded system. Suffice to say that a external oscillator will almost always have way superior precision than any internal RC oscillator. Cause that what internal oscillator essentially is, just a capacitor (C) and resistor (R). However as it is almost always is in engineering the cost of precision is burdened with additional circuit complexity. And precision is what you really want because the clock signal from the oscillator (either internal or external) will be divided and/or multiplied by numerous pre- and post-scalers in any microcontroller along with all the clock inaccuracies. So... remember in the previous posts how we used the __system_stm32f4xx.c__ that came with the STM32Cube library? It contained all the code necessary to initialize the stm32 to use the internal oscillator. It was enough to get us going for what we wanted to do. However now we can throw this file to the bin. We gonna make our own startup file. Lets name it...I dunno...__system_setup.c__ and within it we will put the following:

``` c
#include <stm32f4xx.h>
#define VECT_TAB_OFFSET 0x00
#define PLLP 4
#define PLLN 320
#define PLLM 8
#define PLLQ 7

void SystemInit(void){
	/* Disable main PLL */
	RCC->CR &= ~(RCC_CR_PLLON);
	/* Wait for PLL to stabilize */
	while ((RCC->CR & RCC_CR_PLLRDY) != 0);
	/* HSE as PLL input */
	RCC->PLLCFGR = PLLM | (PLLN<<6) | (((PLLP>>1) -1)<<16)|(RCC_PLLCFGR_PLLSRC_HSE) | (PLLQ<<24); 
	RCC->APB1ENR |= RCC_APB1ENR_PWREN;
	PWR->CR &= (uint32_t)~(PWR_CR_VOS);
	
	/* Reset CFGR register */
	RCC->CFGR = 0x00000000;
	/* PLL used as system clock and periferal stuff DIV2*/
	RCC->CFGR |= (RCC_CFGR_HPRE_DIV1 | 	RCC_CFGR_PPRE1_DIV2);
	/* enable external oscillator*/
	RCC->CR |= RCC_CR_HSEON;
	/* wait for external clock to settle*/
	while((RCC->CR & RCC_CR_HSERDY) == 0);
	/* Configure Flash prefetch, Instruction cache, Data cache and wait state */
	FLASH->ACR = FLASH_ACR_PRFTEN |FLASH_ACR_ICEN |FLASH_ACR_DCEN |FLASH_ACR_LATENCY_2WS;
	/* start PLL */
	RCC->CR |= RCC_CR_PLLON;
	while ((RCC->CR & RCC_CR_PLLRDY) == 0);
	/* with PLL as system clock */
	RCC->CFGR &= (uint32_t)((uint32_t)~(RCC_CFGR_SW));
	RCC->CFGR |= RCC_CFGR_SW_PLL;
	/* wait for it to settle */
	while ((RCC->CFGR & (uint32_t)RCC_CFGR_SWS ) != RCC_CFGR_SWS_PLL);
}
```
Not that hard right? The `<<` and `>>` here and there might look slightly intimidating but this just a handy way of setting bits. And stuff here is getting more and more interesting. First thing to note here, the name of the function. Its called SystemInit(). If you name it this way all will be fine however if for whatever reason you might wish to change this name you will need to edit one more file. The `startup_stm32f401xc.s`. In this file, around line 111 there is a piece of assembly code that calls this particular function to perform the system initialization. See how all the files in your project directory are tied in together? So like I said if you experience some irrational hatred towards this particular character combination feel free to renamed but just remember to modify the startup_stm32f401xc.s.

Next interesting thing are the defines at the top of the file. The ones with PLL in the name. So what are they? If you look into the datasheet (which you should be doing constantly to prove how wrong I am), you will see in the "RCC PLL configuration register" section (chapter 6.3.2 in this [stm32f401xC programming manual](http://www.st.com/web/en/resource/technical/document/reference_manual/DM00096844.pdf) datasheet) an equation:
<img src="http://chart.apis.google.com/chart?cht=tx&amp;chl=f(VCO%20clock)%20%3D%20f(PLL%20input%20clock)%20*(PLLN%2FPLLM)%0A" alt="f(VCO clock) = f(PLL input clock) *(PLLN/PLLM)\r\n" align="absmiddle" />

<img src="http://chart.apis.google.com/chart?cht=tx&amp;chl=f(PLL%20general%20clock%20output)%20%3D%20f(VCO%20clock)%20%2F%20PLLP" alt="f(PLL general clock output) = f(VCO clock) / PLLP" align="absmiddle" />

ST has a windows tool to calculate those values but, this is not that hard to do it ourselves.

* f(PLL general clock output) - is the frequency of your system clock. Its essentially the speed at which your microcontroller will be operating.
*f(PLL clock input) - is the frequency of the oscillator used, either external or internal

The equation is straightforward. Just substitute the frequency at which you want your system to run at (80MHz in this case) and assume assume PLLM as 8MHz so that it reduces itself nicely with the external oscillator frequency. So we can now calculate the values backwards:
80MHz = fvco/PLLP
fvco = 320
320 = 8MHz * PLLN/8
PLLN = 320

And its done now, you can now delete the __system_stm32f4xx.c__ from the project directory and add the `system_setup.c` instead. Assuming that you are using the Makefile from my earlier post you should be able to simply run make and the project will build. Let me know in the comments what do you think about the posts and if I made some mistakes you can point a finger at me and laugh... Next time I will for sure show you how to configure the pins to generate the interrupts and maybe even show you how to configure the systick timer and if I'm feeling really, really adventurous we will configure the systick to generate the the interrupts as well :)
