#![recursion_limit = "2048"]
#[macro_use]
extern crate lazy_static;
use chrono::Datelike;
use components::post_meta::PostMeta;
use dotenv;
use pages::{about::About, home::Home, post::Post, posts::PostMetaList};
use std::collections::HashMap;
use yew::{prelude::*, start_app_in_element, utils};
use yew_router::prelude::*;

mod components;
mod fetch;
mod markdown;
mod pages;
mod read_time;

const POSTS: &str = include_str!("../content/posts.json");
lazy_static! {
    pub static ref BASE_URL: String = env::var("BASE_URL")
        .map_or("https://versbinarii.com/content/".to_string(), |b| b);
    static ref POSTS_META: Vec<PostMeta> = serde_json::from_str(POSTS).unwrap();
}

#[derive(Routable, Clone, PartialEq)]
enum WebRoute {
    #[at("/posts")]
    Posts,
    #[at("/post/:file_path")]
    Post { file_path: String },
    #[at("/about")]
    About,
    #[not_found]
    #[at("/404")]
    PageNotFound,
    #[at("/")]
    Home,
}

struct Website;

impl Component for Website {
    type Message = ();
    type Properties = ();
    fn create(_: &Context<Self>) -> Self {
        Self
    }

    fn update(&mut self, _ctx: &Context<Self>, _: Self::Message) -> bool {
        true
    }

    fn view(&self, _: &Context<Self>) -> Html {
        html! {
            <main>
                <Router<WebRoute>
                    render={Router::render(Website::switch)}
                />
            </main>
        }
    }
}

impl Website {
    fn switch(route: &WebRoute) -> Html {
        match route {
            WebRoute::Posts => {
                let mut groups = HashMap::new();
                POSTS_META.to_vec().into_iter().for_each(|post| {
                    let group = groups
                        .entry(post.date.year().to_string())
                        .or_insert(vec![]);
                    group.push(post.clone());
                    group.sort_by(|a, b| b.date.cmp(&a.date));
                });

                html! {<PostMetaList posts={groups}/>}
            }
            WebRoute::Post { file_path } => {
                let mut posts = POSTS_META.to_vec();
                posts.sort_by(|a, b| a.date.cmp(&b.date));
                let (id, post) = posts
                    .iter()
                    .enumerate()
                    .find(|(_, e)| &e.file == file_path)
                    .unwrap();

                // Get previous post if possible but if we reached end
                // force `get` to return None
                let prev_post = posts
                    .get(id.checked_sub(1).unwrap_or(usize::MAX))
                    .and_then(|v| Some(v.to_owned()));
                let next_post =
                    posts.get(id + 1).and_then(|v| Some(v.to_owned()));
                html! {<Post post={post.clone()} prev={prev_post} next={next_post} />}
            }
            WebRoute::Home => html! {<Home/>},
            WebRoute::About => html! { <About /> },
            WebRoute::PageNotFound => html! {"404 Not found"},
        }
    }
}

pub fn main() {
    let document = utils::document();
    let body = document.query_selector("#mount").unwrap().unwrap();

    start_app_in_element::<Website>(body);
}
