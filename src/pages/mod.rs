use crate::markdown::MarkdownFile;
use yew::Html;

pub mod about;
pub mod home;
pub mod post;
pub mod posts;

pub fn render_markdown_content(post_content: &str) -> Html {
    let mf = MarkdownFile::render_content(post_content);
    let div = yew::utils::document().create_element("div").unwrap();
    div.set_inner_html(&mf);
    Html::VRef(div.into())
}
