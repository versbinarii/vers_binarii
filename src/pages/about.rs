use crate::{
    components::header::Header, fetch::fetch_post,
    pages::render_markdown_content,
};
use yew::prelude::*;

pub struct About {
    content: Option<String>,
}

impl About {
    fn render_content(&self) -> Html {
        match &self.content {
            None => html! {},
            Some(content) => render_markdown_content(&content),
        }
    }
}

pub enum Msg {
    FetchOk(String),
    FetchFail(String),
}

impl Component for About {
    type Properties = ();
    type Message = Msg;

    fn create(ctx: &Context<Self>) -> Self {
        ctx.link().send_future(async {
            match fetch_post("about.md").await {
                Ok(post) => Msg::FetchOk(post),
                Err(err) => Msg::FetchFail(err.to_string()),
            }
        });

        Self { content: None }
    }

    fn update(&mut self, _: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::FetchOk(content) => {
                self.content = Some(content);
                true
            }
            Msg::FetchFail(_) => false,
        }
    }

    fn view(&self, _: &Context<Self>) -> Html {
        html! {
            <>
            <Header />

            <main class="site-main section-inner animated fadeIn faster">
                <article class="thin">
                    <div class="content">
                        {self.render_content()}
                    </div>
                </article>
            </main>
            </>
        }
    }
}
