use crate::{
    components::social::{SocialIcon, SocialIconName},
    WebRoute,
};
use yew::prelude::*;
use yew_router::prelude::*;

pub struct Home;

impl Component for Home {
    type Message = ();
    type Properties = ();

    fn create(_: &Context<Self>) -> Self {
        Self
    }

    fn update(&mut self, _: &Context<Self>, _: Self::Message) -> bool {
        false
    }

    fn view(&self, _: &Context<Self>) -> Html {
        html! {
        <div id="spotlight" class="animated fadeIn">
              <div id="home-center">
                <h1 id="home-title">{"VersBinarii"}</h1>
                <p id="home-subtitle">{"Some profound and catchy statement"}</p>
                <div id="home-social">
                    <SocialIcon name={SocialIconName::Github} url="https://github.com/VersBinarii"/>
                    <SocialIcon name={SocialIconName::Gitlab} url="https://gitlab.com/versbinarii"/>
                    <SocialIcon name={SocialIconName::Youtube} url="https://www.youtube.com/channel/UCgLxnJi8BU476a3uZO29H4w"/>
                    <SocialIcon name={SocialIconName::Email} url="versbinarii@gmail.com"/>
                    <SocialIcon name={SocialIconName::LinkedIn} url="https://www.linkedin.com/in/catzrulz/"/>
                </div>
                <nav id="home-nav" class="site-nav">
                    <Link<WebRoute> route={WebRoute::Posts}>
                        {"Posts"}
                    </Link<WebRoute>>

                    <Link<WebRoute> route={WebRoute::About}>
                        {"About"}
                    </Link<WebRoute>>
                </nav>
              </div>
            </div>
                }
    }
}
