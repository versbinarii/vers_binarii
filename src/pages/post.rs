use crate::components::{
    header::Header,
    post_meta::PostMeta,
    post_navigation::{PostNav, PostNavDirection},
    toc::Toc,
};
use crate::fetch::fetch_post;
use crate::markdown::MarkdownFile;
use crate::pages::render_markdown_content;
use crate::read_time::reading_time;
use gloo::console;
use yew::prelude::*;

pub enum Msg {
    FetchOk(String),
    FetchFail(String),
    TocToggle,
}

#[derive(Properties, Clone, PartialEq)]
pub struct PostData {
    pub post: PostMeta,
    pub next: Option<PostMeta>,
    pub prev: Option<PostMeta>,
}

pub struct Post {
    pub props: PostData,
    post_content: Option<String>,
    toc_visible: bool,
}

impl Post {
    fn render_content(&self) -> Html {
        match self.post_content.as_ref() {
            None => html! {<></>},
            Some(content) => render_markdown_content(content),
        }
    }

    fn render_reading_time(&self) -> Html {
        match self.post_content.as_ref() {
            None => html! {<></>},
            Some(content) => {
                html! {
                     {&format!("{} minute read", reading_time(content))}
                }
            }
        }
    }
    fn render_word_count(&self) -> Html {
        match self.post_content.as_ref() {
            None => html! {<></>},
            Some(content) => {
                html! {
                     {&format!("{} words", content.split(' ').count())}
                }
            }
        }
    }
    fn render_toc(&self) -> Html {
        match self.toc_visible {
            true => {
                let tos = MarkdownFile::render_tos(
                    &self.post_content.as_ref().unwrap_or(&String::new()),
                );
                html! {
                    <Toc elements={tos} />
                }
            }
            false => {
                html! {<></>}
            }
        }
    }
}

impl Component for Post {
    type Properties = PostData;
    type Message = Msg;

    fn create(ctx: &Context<Self>) -> Self {
        let url = ctx.props().post.file.clone();
        ctx.link().send_future(async move {
            match fetch_post(&url).await {
                Ok(post) => Msg::FetchOk(post),
                Err(err) => Msg::FetchFail(err.to_string()),
            }
        });

        Self {
            props: ctx.props().to_owned(),
            post_content: None,
            toc_visible: false,
        }
    }

    fn update(&mut self, _: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::FetchFail(err) => {
                console::error!(&err);
                false
            }
            Msg::FetchOk(content) => {
                self.post_content = Some(content);
                true
            }
            Msg::TocToggle => {
                self.toc_visible = !self.toc_visible;
                true
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        html! {
        <>
            <Header />
            <main class="site-main section-inner animated fadeIn faster">
                <article class="thin">
                    <header class="post-header">
                        <div class="post-meta">
                        <span>{self.props.post.date.format("%b %d, %Y")}</span>
                        <small> { " - " }
                            <span class="reading-time" title="Estimated read time">
                                {self.render_reading_time()}
                            </span>
                        </small>
                        <button id="toc-btn" onclick={ctx.link().callback(|_|Msg::TocToggle)} class="hdr-btn desktop-only-ib">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                            viewBox="0 0 24 24" fill="none" stroke="currentColor"
                            stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                            class="feather feather-list">
                            <line x1="8" y1="6" x2="21" y2="6"></line>
                            <line x1="8" y1="12" x2="21" y2="12"></line>
                            <line x1="8" y1="18" x2="21" y2="18"></line>
                            <line x1="3" y1="6" x2="3" y2="6"></line>
                            <line x1="3" y1="12" x2="3" y2="12"></line>
                            <line x1="3" y1="18" x2="3" y2="18"></line>
                            </svg>
                        </button>
                        </div>
                        <h1>{&self.props.post.title}</h1>
                    </header>
                    <div class="content">
                        {self.render_content()}
                    </div>
                    <hr class="post-end"/>
                    <footer class="post-info">
                        <p>
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                stroke-linecap="round" stroke-linejoin="round" class="feather feather-tag meta-icon">
                                <path d="M20.59 13.41l-7.17 7.17a2 2 0 0 1-2.83 0L2 12V2h10l8.59 8.59a2 2 0 0 1 0 2.82z"></path>
                                <line x1="7" y1="7" x2="7" y2="7"></line>
                            </svg>
                            <span class="tag"><a href="tags&#x2F;linux">{"linux"}</a></span>
                            <span class="tag"><a href="tags&#x2F;arch&#x2F;">{"arch"}</a></span>
                            <span class="tag"><a href="tags&#x2F;sway&#x2F;">{"sway"}</a></span>
                            <span class="tag"><a href="tags&#x2F;wayland&#x2F;">{"wayland"}</a></span>
                        </p>
                        <p>
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text">
                                <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>
                                <polyline points="14 2 14 8 20 8"></polyline>
                                <line x1="16" y1="13" x2="8" y2="13"></line>
                                <line x1="16" y1="17" x2="8" y2="17"></line>
                                <polyline points="10 9 9 9 8 9"></polyline>
                            </svg>
                            {self.render_word_count()}
                        </p>
                        <p>
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar">
                            <rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect>
                            <line x1="16" y1="2" x2="16" y2="6"></line>
                            <line x1="8" y1="2" x2="8" y2="6"></line>
                            <line x1="3" y1="10" x2="21" y2="10"></line>
                            </svg>
                            {self.props.post.date.format("%Y-%m-%d")}
                        </p>
                    </footer>
                </article>

                {self.render_toc()}


          <div class="post-nav thin">
          {for self.props.next.iter()
              .map(|post|html!{
                  <PostNav
                      label="Newer"
                      direction={PostNavDirection::Next}
                        post_url={post.file.clone()}
                        post_title={post.title.clone()} />})}
          {for self.props.prev.iter()
              .map(|post|html!{
                  <PostNav
                      label="Older"
                      direction={PostNavDirection::Prev}
                        post_url={post.file.clone()}
                        post_title={post.title.clone()} />})}
          </div>
        </main>
                    </>
                }
    }
}
