use crate::components::{header::Header, post_meta::PostMeta};
use crate::WebRoute;
use std::collections::HashMap;
use std::iter::FromIterator;
use yew::prelude::*;
use yew_router::prelude::*;

#[derive(Properties, Clone, Debug, PartialEq)]
pub struct Properties {
    pub posts: HashMap<String, Vec<PostMeta>>,
}

pub struct PostMetaList {
    posts: Vec<(String, Vec<PostMeta>)>,
}

impl Component for PostMetaList {
    type Message = ();
    type Properties = Properties;

    fn create(ctx: &Context<Self>) -> Self {
        /* Sort the grouped posts by year */
        let mut v = Vec::from_iter(ctx.props().posts.clone());
        v.sort_by(|a, b| b.0.cmp(&a.0));
        Self { posts: v }
    }

    fn update(&mut self, _: &Context<Self>, _: Self::Message) -> bool {
        false
    }

    fn view(&self, _: &Context<Self>) -> Html {
        html! {
            <>
                <Header />
                <div id="mobile-menu" class="animated fast">
                    <ul>
                        <li>
                            <Link<WebRoute> route={WebRoute::Posts}>
                                {"Posts"}
                            </Link<WebRoute>>
                        </li>
                        <li>
                            <Link<WebRoute> route={WebRoute::About}>
                                {"About"}
                            </Link<WebRoute>>
                        </li>
                    </ul>
                </div>

                <main class="site-main section-inner thin animated fadeIn faster">
                    <h1>{"Posts"}</h1>
                    {for self.posts.iter().map(|(year, pm)|{
                        html!{
                            <div class="posts-group">
                                <div class="post-year">{year}</div>
                                <ul class="posts-list">
                                {for pm.iter().map(|p|{
                                    html!{
                                        <li class="post-item">
                                            <PostMeta file={p.file.clone()}
                                                date={p.date.clone()}
                                                title={p.title.clone()}
                                                tags={p.tags.clone()}
                                                toc={p.toc.clone()}/>
                                        </li>
                                    }
                                })}
                                </ul>
                            </div>
                        }
                    })}
                </main>
            </>
        }
    }
}
