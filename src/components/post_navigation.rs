use std::borrow::Cow;

use yew::prelude::*;

#[derive(Clone, PartialEq)]
pub enum PostNavDirection {
    Next,
    Prev,
}

#[derive(Properties, Clone, PartialEq)]
pub struct Props {
    pub label: Cow<'static, str>,
    pub direction: PostNavDirection,
    pub post_title: Cow<'static, str>,
    pub post_url: Cow<'static, str>,
}

pub struct PostNav {
    props: Props,
}

impl Component for PostNav {
    type Message = ();
    type Properties = Props;

    fn create(ctx: &Context<Self>) -> Self {
        Self {
            props: ctx.props().to_owned(),
        }
    }

    fn update(&mut self, _ctx: &Context<Self>, _msg: Self::Message) -> bool {
        false
    }

    fn view(&self, _ctx: &Context<Self>) -> yew::Html {
        match self.props.direction {
            PostNavDirection::Next => {
                html! {
                <a class="next-post" href={&self.props.post_url}>
                    <span class="post-nav-label">
                        <svg xmlns="http://www.w3.org/2000/svg"
                            width="24" height="24"
                            viewBox="0 0 24 24" fill="none"
                            stroke="currentColor" stroke-width="2"
                            stroke-linecap="round" stroke-linejoin="round"
                            class="feather feather-arrow-left">
                                <line x1="19" y1="12" x2="5" y2="12"></line>
                                <polyline points="12 19 5 12 12 5"></polyline>
                        </svg>
                        {&self.props.label}
                    </span>
                    <br/>
                    <span>{&self.props.post_title}</span>
                </a>
                }
            }
            PostNavDirection::Prev => {
                html! {
                <a class="prev-post" href={&self.props.post_url}>
                  <span class="post-nav-label">
                    {&self.props.label}
                    <svg xmlns="http://www.w3.org/2000/svg"
                        width="24" height="24"
                        viewBox="0 0 24 24" fill="none"
                        stroke="currentColor" stroke-width="2"
                        stroke-linecap="round" stroke-linejoin="round"
                        class="feather feather-arrow-right">
                            <line x1="5" y1="12" x2="19" y2="12"></line>
                            <polyline points="12 5 19 12 12 19"></polyline>
                    </svg>
                </span>
                <br/>
                <span>{&self.props.post_title}</span>
                </a>
                    }
            }
        }
    }
}
