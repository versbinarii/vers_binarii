use crate::{
    components::social::{SocialIcon, SocialIconName},
    WebRoute,
};
use yew::prelude::*;
use yew_router::prelude::*;

pub struct Header;

impl Component for Header {
    type Properties = ();
    type Message = ();
    fn create(_ctx: &Context<Self>) -> Self {
        Self
    }
    fn update(&mut self, _ctx: &Context<Self>, _: Self::Message) -> bool {
        false
    }
    fn view(&self, _ctx: &Context<Self>) -> Html {
        html! {
        <header id="site-header" class="animated slideInUp faster">
            <div class="hdr-wrapper section-inner">
                <div class="hdr-left">
                  <div class="site-branding">
                    <Link<WebRoute> route={WebRoute::Home}>
                        {"VersBinarii"}
                    </Link<WebRoute>>
                  </div>
                  <nav class="site-nav hide-in-mobile">

                        <Link<WebRoute> route={WebRoute::Posts}>
                            {"Posts"}
                        </Link<WebRoute>>

                        <Link<WebRoute> route={WebRoute::About}>
                            {"About"}
                        </Link<WebRoute>>

                  </nav>
                </div>
                <div class="hdr-right hdr-icons">
                    <span class="hdr-social hide-in-mobile">

                        <SocialIcon name={SocialIconName::Github} url="https://github.com/VersBinarii"/>
                        <SocialIcon name={SocialIconName::Gitlab} url="https://gitlab.com/versbinarii"/>
                        <SocialIcon name={SocialIconName::Youtube} url="https://www.youtube.com/channel/UCgLxnJi8BU476a3uZO29H4w"/>
                        <SocialIcon name={SocialIconName::Email} url="versbinarii@gmail.com"/>
                        <SocialIcon name={SocialIconName::LinkedIn} url="https://www.linkedin.com/in/catzrulz/"/>

                    </span>
                  <button id="menu-btn" class="hdr-btn" title="Menu">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      stroke="currentColor"
                      stroke-width="2"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      class="feather feather-menu"
                    >
                      <line x1="3" y1="12" x2="21" y2="12"></line>
                      <line x1="3" y1="6" x2="21" y2="6"></line>
                      <line x1="3" y1="18" x2="21" y2="18"></line>
                    </svg>
                  </button>
                </div>
              </div>
            </header>
            }
    }
}
