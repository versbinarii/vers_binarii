use crate::components::toc_list::TocList;
use yew::{html::Properties, macros::html, Component, Context};

#[derive(Properties, Clone, PartialEq)]
pub struct Props {
    pub elements: Vec<Vec<String>>,
}

pub struct Toc {
    props: Props,
}

impl Component for Toc {
    type Message = ();
    type Properties = Props;

    fn create(ctx: &Context<Self>) -> Self {
        Self {
            props: ctx.props().to_owned(),
        }
    }

    fn update(&mut self, _: &Context<Self>, _msg: Self::Message) -> bool {
        false
    }

    fn view(&self, _: &Context<Self>) -> yew::Html {
        html! {
            <aside id="toc">
                <div id="TableOfContents">
                    <div class="toc-title">
                    </div>
                    <TocList elements={self.props.elements.clone()}/>
                </div>
            </aside>
        }
    }
}
