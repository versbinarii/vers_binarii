use std::borrow::Cow;

use crate::WebRoute;
use chrono::NaiveDate;
use serde::Deserialize;
use yew::prelude::*;
use yew_router::prelude::*;

#[derive(Deserialize, Clone, Debug, Properties, PartialEq)]
pub struct PostMeta {
    pub file: Cow<'static, str>,
    // This should be Chrono Date type but
    // Chrono does not work well with wasm yet
    #[serde(with = "blog_date_format")]
    pub date: NaiveDate,
    pub title: Cow<'static, str>,
    #[prop_or(vec![])]
    pub tags: Vec<String>,
    #[prop_or(false)]
    pub toc: bool,
}

impl Component for PostMeta {
    type Message = ();
    type Properties = Self;

    fn create(ctx: &Context<Self>) -> Self {
        ctx.props().to_owned()
    }

    fn update(&mut self, _ctx: &Context<Self>, _: Self::Message) -> bool {
        false
    }

    fn view(&self, _ctx: &Context<Self>) -> Html {
        html! {
            <>
                <Link<WebRoute> route={ WebRoute::Post{
                                            file_path: self.file.to_string()
                                        }}>
                <span class="post-title">{&self.title}</span>
                <span class="post-day">{&self.date}</span>
                </Link<WebRoute>>
            </>
        }
    }
}

mod blog_date_format {

    use chrono::NaiveDate;
    use serde::{self, Deserialize, Deserializer};

    const FORMAT: &'static str = "%d/%m/%Y";

    pub fn deserialize<'de, D>(deserializer: D) -> Result<NaiveDate, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;
        NaiveDate::parse_from_str(&s, FORMAT).map_err(serde::de::Error::custom)
    }
}
