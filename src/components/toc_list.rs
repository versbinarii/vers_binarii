use yew::{html::Properties, macros::html, Component, Context};

#[derive(Properties, Clone, PartialEq)]
pub struct Props {
    pub elements: Vec<Vec<String>>,
}

pub struct TocList {
    props: Props,
}

impl Component for TocList {
    type Message = ();
    type Properties = Props;

    fn create(ctx: &Context<Self>) -> Self {
        Self {
            props: ctx.props().to_owned(),
        }
    }

    fn update(&mut self, _: &Context<Self>, _msg: Self::Message) -> bool {
        false
    }

    fn view(&self, _: &Context<Self>) -> yew::Html {
        html! {
            <ul>
                {for self.props.elements.iter().map(|elem|{
                    html!{
                        <li>
                            <a href={format!("#{}",&elem[0])}>
                                {capitalize_headings(&elem[0])}
                            </a>
                             <ul>
                                {for elem[1..].iter().map(|sub_elem|{html!{<li>{sub_elem}</li>}})}
                            </ul>
                        </li>
                    }
                })}
            </ul>
        }
    }
}

fn capitalize_headings(heading: &str) -> String {
    let mut s = heading.to_string();

    if let Some(ss) = s.get_mut(0..1) {
        ss.make_ascii_uppercase();
    }

    s.replace("-", " ")
}
