pub fn reading_time(text: &str) -> usize {
    text.split(' ').count() / 275 + 1
}
