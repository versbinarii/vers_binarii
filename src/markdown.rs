use gloo::console;
use pulldown_cmark::{html, Event, Parser};

pub struct MarkdownFile;

impl MarkdownFile {
    pub fn render_content(content: &str) -> String {
        let mut html_out = String::new();

        html::push_html(&mut html_out, Parser::new(content));
        html_out
    }

    pub fn render_tos(content: &str) -> Vec<Vec<String>> {
        let mut tos = vec![];
        let mut tos_indented = vec![];

        Parser::new(content).for_each(|event| match event {
            Event::Html(content) => {
                console::log!(&format!("{:?}", content));
                let indent: u32 = content[2usize..3usize].parse().unwrap_or(0); //Get the header size
                let indent = if indent == 3 { true } else { false };
                let mut heading_split = content.split('\"');
                let heading = heading_split.nth(1).unwrap_or("");
                tos_indented.push(heading.to_string());
                if !indent {
                    tos.push(tos_indented.clone());
                    tos_indented.clear();
                }
            }
            _ => (),
        });
        tos
    }
}
